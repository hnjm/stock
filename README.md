[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Pipeline Status](https://gitlab.com/tawasta/odoo/stock/badges/14.0-dev/pipeline.svg)](https://gitlab.com/tawasta/odoo/stock/-/pipelines/)

Stock
=====
Stock Addons for Odoo.

[//]: # (addons)

Available addons
----------------
addon | version | maintainers | summary
--- | --- | --- | ---
[product_supplierinfo_for_customer_picking_filter_fix](product_supplierinfo_for_customer_picking_filter_fix/) | 14.0.1.0.0 |  | product_supplierinfo_for_customer_picking module's filter fix
[stock_auditlog_rules](stock_auditlog_rules/) | 14.0.1.0.0 |  | Adds audit log rules for stock.warehouse and stock.location
[stock_disable_invoice_shipping_on_delivery](stock_disable_invoice_shipping_on_delivery/) | 14.0.1.0.0 |  | Disable picking auto-generated delivery SO lines
[stock_dispatch_note_report_template](stock_dispatch_note_report_template/) | 14.0.1.0.4 |  | Stock Dispatch Note Template
[stock_hide_packing_buttons](stock_hide_packing_buttons/) | 14.0.1.0.0 |  | Hides "put in pack" buttons
[stock_inventory_adjustment_domain_company](stock_inventory_adjustment_domain_company/) | 14.0.1.0.1 |  | Show stock locations without company in inventory adjustments
[stock_inventory_continue_inventory_tree_order](stock_inventory_continue_inventory_tree_order/) | 14.0.1.0.1 |  | Inventory list view - order by product_id
[stock_inventory_line_order](stock_inventory_line_order/) | 14.0.1.0.0 |  | Sort inventory lines by default code
[stock_inventory_products_tree](stock_inventory_products_tree/) | 14.0.1.1.0 |  | Show products in stock inventory as tree view
[stock_move_pivot_report_sh_product_tag](stock_move_pivot_report_sh_product_tag/) | 14.0.1.0.0 |  | Group by SH product tags in Stock Move pivot view
[stock_move_sale_kit](stock_move_sale_kit/) | 14.0.1.0.0 |  | Sale kits to stock move lines
[stock_move_search](stock_move_search/) | 14.0.1.0.0 |  | Additional Search options for Stock Move
[stock_package_sticker](stock_package_sticker/) | 14.0.1.0.1 |  | Stock Package Sticker
[stock_picking_bypass_reservation](stock_picking_bypass_reservation/) | 14.0.1.0.0 |  | Stock Picking Bypass Reservation
[stock_picking_comment](stock_picking_comment/) | 14.0.1.0.1 |  | External comment for stock picking Delivery Slip and Picking Operations
[stock_picking_compatible_mass_print_and_sudo_process](stock_picking_compatible_mass_print_and_sudo_process/) | 14.0.1.0.0 |  | Make mass action print and process as sudo compatible
[stock_picking_country_group_text](stock_picking_country_group_text/) | 14.0.1.0.0 |  | Get stock picking report text from country groups setting
[stock_picking_create_manufacturing_order_from_move](stock_picking_create_manufacturing_order_from_move/) | 14.0.1.0.0 |  | Create Manufacturing order from stock picking move
[stock_picking_customer_reference](stock_picking_customer_reference/) | 14.0.1.0.0 |  | Stock Picking Customer Reference
[stock_picking_dispatch_fields](stock_picking_dispatch_fields/) | 14.0.1.0.0 |  | Stock Picking fields for Dispatch Note
[stock_picking_mass_action_complete_transfer](stock_picking_mass_action_complete_transfer/) | 14.0.1.0.1 |  | Stock Picking Mass action - complete transfer
[stock_picking_mass_action_print_pdf](stock_picking_mass_action_print_pdf/) | 14.0.1.0.1 |  | Stock Picking Mass action - Print PDF
[stock_picking_operations_show_name](stock_picking_operations_show_name/) | 14.0.1.0.0 |  | Show name field on Stock Picking operations
[stock_picking_override_values](stock_picking_override_values/) | 14.0.1.0.0 |  | Allow overriding new picking values with system parameters
[stock_picking_printed](stock_picking_printed/) | 14.0.1.0.0 |  | Mark stock pickings as printed after printing picking list
[stock_picking_process_as_sudo](stock_picking_process_as_sudo/) | 14.0.1.0.0 |  | Process Stock Pickings as Admin user
[stock_picking_receipt_set_all_moves_done](stock_picking_receipt_set_all_moves_done/) | 14.0.1.0.0 |  | Use button to set all moves as done on receipt
[stock_picking_reinvoice](stock_picking_reinvoice/) | 14.0.1.0.2 |  | Allow making invoices from stock pickings
[stock_picking_reserve_chosen_moves](stock_picking_reserve_chosen_moves/) | 14.0.1.0.0 |  | Select manually the moves to be reserved on deliveries
[stock_picking_sale_order_id](stock_picking_sale_order_id/) | 14.0.1.0.0 |  | Get Sale Order to Stock Picking from Purchase Order
[stock_picking_sale_order_partner_id](stock_picking_sale_order_partner_id/) | 14.0.1.0.0 |  | Get Sale Order Partner to Stock Picking from Purchase Order
[stock_picking_sort_by_print_and_scheduled_date](stock_picking_sort_by_print_and_scheduled_date/) | 14.0.1.0.0 |  | Stock Picking sort by printed and Scheduled Date
[stock_picking_tree_date_deadline_as_date](stock_picking_tree_date_deadline_as_date/) | 14.0.1.0.0 |  | Show date deadline as date in stock picking tree
[stock_picking_tree_date_done](stock_picking_tree_date_done/) | 14.0.1.0.0 |  | Stock Picking date done in tree view
[stock_picking_tree_effective_date](stock_picking_tree_effective_date/) | 14.0.1.0.0 |  | Adds effective date from Sale Order to picking list view
[stock_picking_tree_scheduled_date_as_date](stock_picking_tree_scheduled_date_as_date/) | 14.0.1.0.0 |  | Show scheduled date as date in stock picking tree
[stock_picking_unreserve_automatically](stock_picking_unreserve_automatically/) | 14.0.1.0.0 |  | Set sale orders to automatically unreserve deliveries
[stock_product_qty_available_unreserved](stock_product_qty_available_unreserved/) | 14.0.1.0.0 |  | Add unreserved available (on hand - reserved)
[stock_report_add_description_to_picking](stock_report_add_description_to_picking/) | 14.0.1.0.0 |  | Adds Description column to picking report
[stock_report_carrier](stock_report_carrier/) | 14.0.1.0.1 |  | Delivery slip Carrier
[stock_report_carrier_tracking_ref](stock_report_carrier_tracking_ref/) | 14.0.1.0.0 |  | Adds tracking reference to delivery slip
[stock_report_code_as_name_stock_picking](stock_report_code_as_name_stock_picking/) | 14.0.1.0.0 |  | Replace Product name with product code
[stock_report_customer_address](stock_report_customer_address/) | 14.0.1.0.0 |  | Stock Report Customer Address
[stock_report_customer_reference](stock_report_customer_reference/) | 14.0.1.0.0 |  | Stock Report Customer Reference
[stock_report_customer_reference_under_address](stock_report_customer_reference_under_address/) | 14.0.1.0.1 |  | Customer Reference under address
[stock_report_date_done](stock_report_date_done/) | 14.0.1.0.0 |  | Stock Picking Report Date of Transfer
[stock_report_element_sizes](stock_report_element_sizes/) | 14.0.1.0.2 |  | Stock Report element size changes
[stock_report_enable_translation_by_partner](stock_report_enable_translation_by_partner/) | 14.0.1.0.0 |  | Enable report translation by partner language
[stock_report_handler](stock_report_handler/) | 14.0.0.1.0 |  | Delivery Slip - Handler
[stock_report_invoice_and_delivery_address](stock_report_invoice_and_delivery_address/) | 14.0.1.0.5 |  | Adds Invoice and Delivery addresses to delivery slip
[stock_report_item_count](stock_report_item_count/) | 14.0.1.0.1 |  | Add item count to stock report lines
[stock_report_our_reference](stock_report_our_reference/) | 14.0.1.0.0 |  | Stock Picking and Delivery Slip Report Our Reference
[stock_report_payment_terms](stock_report_payment_terms/) | 14.0.1.0.0 |  | Adds payment terms to Delivery Slip
[stock_report_picking_customer_address](stock_report_picking_customer_address/) | 14.0.1.0.0 |  | Stock Report picking Customer Address details
[stock_report_picking_hide_footer](stock_report_picking_hide_footer/) | 14.0.1.0.0 |  | Hide footer on Picking Operations
[stock_report_picking_invoice_and_delivery_address](stock_report_picking_invoice_and_delivery_address/) | 14.0.1.0.5 |  | Adds Invoice and Delivery addresses to Picking list
[stock_report_picking_product_location](stock_report_picking_product_location/) | 14.0.1.0.1 |  | Rack and Row are shown on picking list
[stock_report_product_customer_code](stock_report_product_customer_code/) | 14.0.1.0.0 |  | Place Product Customer code to picking print
[stock_report_quantity_decimals](stock_report_quantity_decimals/) | 14.0.1.0.0 |  | Modifications to Stock Reports' decimal precision
[stock_report_scheduled_date_as_date_only](stock_report_scheduled_date_as_date_only/) | 14.0.1.0.0 |  | Stock Picking and Delivery Slip Report Scheduled Date as Date only
[stock_report_title](stock_report_title/) | 14.0.1.0.0 |  | Stock Picking and Delivery Slip Report Title
[stock_report_total_weight](stock_report_total_weight/) | 14.0.1.0.1 |  | Add total weight to stock report lines
[stock_report_week_of_shipment](stock_report_week_of_shipment/) | 14.0.1.0.0 |  | Add week of shipment to Picking report
[stock_report_year_of_shipment](stock_report_year_of_shipment/) | 14.0.1.0.0 |  | Add year of shipment to delivery slip
[stock_valuation_layer_archive](stock_valuation_layer_archive/) | 14.0.1.0.1 |  | Allows archiving inventory valuation records

[//]: # (end addons)
