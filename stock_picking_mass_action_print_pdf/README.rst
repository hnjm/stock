.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

=====================================
Stock Picking Mass action - Print PDF
=====================================

Enables to print Delivery Slips from all the pickings, which have been
transferred with OCA's stock_picking_mass_action module. Printing will
be done also if some backorders are created.

Configuration
=============
\-

Usage
=====
* Go to Apps to install this module

Known issues / Roadmap
======================
\-

Credits
=======

Contributors
------------

* Timo Kekäläinen <timo.kekalainen@tawasta.fi>

Maintainer
----------

.. image:: http://tawasta.fi/templates/tawastrap/images/logo.png
   :alt: Oy Tawasta OS Technologies Ltd.
   :target: http://tawasta.fi/

This module is maintained by Oy Tawasta OS Technologies Ltd.
